package com.fabioqmarsiaj.condominiumresidents.model.types;

public enum TiposGrupos {
    MORADOR, SINDICO;
}
