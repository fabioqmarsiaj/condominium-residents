package com.fabioqmarsiaj.condominiumresidents.model.exception;

public class GrupoInvalidoException extends RuntimeException {

    public GrupoInvalidoException(String message) {
        super(message);
    }
}
