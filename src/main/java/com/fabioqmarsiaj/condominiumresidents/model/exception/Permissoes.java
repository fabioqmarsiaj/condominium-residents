package com.fabioqmarsiaj.condominiumresidents.model.exception;

public enum Permissoes {
    WRITE, READ, NONE
}
